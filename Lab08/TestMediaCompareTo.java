public class TestMediaCompareTo{
	
	public static void main(String[] args) {
		
		CompactDisc cd1 = new CompactDisc("Tuan", "haha", 20, "The Lion King", "anime", (float) 99.95);
		CompactDisc cd2 = new CompactDisc("Thao", "haha", 16, "The Lion Queen",  "anime", (float) 89.95);
		CompactDisc cd3 = new CompactDisc("Cahoi", "hehe", 15, "Cahoihoang", "music", (float)79.95);
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("Vu", 30, "Song song");
		Collection<Media> listDisc = new java.util.ArrayList<Media>();
		listDisc.add(cd1);
		listDisc.add(cd2);
		listDisc.add(cd3);
		listDisc.add(dvd1);

		Iterator<Media> iterator = listDisc.iterator();
		
	    Collections.sort((List<Media>) listDisc);
	    Iterator<Media> iterator2 = listDisc.iterator();
	    while(iterator2.hasNext()) {
	    	Object obj = iterator2.next();
	    	System.out.println("Playing CD: " + ((Media) obj).getTitle());
			System.out.println("Cost: " + ((Media) obj).getCost());
		}
	}	
}