public abstract class Media implements Comparable<Media>{
     String title;
     String category;
     float cost;
    public Media() {
    }

    public Media(String title) {
        this.title = title;
    }

    public Media(String title, String category) {
        this.title = title;
        this.category = category;
    }

    public Media(String title, String category, float cost) {
        this.title = title;
        this.category = category;
        this.cost = cost;
    }
    
    public String getTitle() {
        return title;
    }

    public String getCategory() {
        return category;
    }

    public float getCost() {
        return cost;
    }
    @Override
    public boolean equals(Object obj) {
    	if(obj instanceof Media) {
    		Media a = (Media)obj;
    		if(this.title.equals(a.title)) return true;
    	}
    	return false;
    }
    
    public int compareTo(Media obj) {
    	return title.compareTo((obj).getTitle());
    	
    }
    public abstract void print();
}