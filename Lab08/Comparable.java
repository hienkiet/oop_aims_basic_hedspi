package project1.media;

public interface Comparable<T> extends java.lang.Comparable<T> {
	 public int compareTo(T obj);
}
