package project1.oder;
import project1.date.MyDate;
import project1.disc.DigitalVideoDisc;
import java.util.Random;
import java.util.StringTokenizer;


public class Oder {
	public static final int MAX_NUMBER_ORDERED = 10;
	public static final int MAX_LIMITTED_ORDERS = 5;
	public  int nbOrders = 0;
	
	public int qtyOdered;
	private MyDate dateOrdered = new MyDate("19/03/2021");
	
	public DigitalVideoDisc itemsOdered[] = new DigitalVideoDisc[MAX_NUMBER_ORDERED];
	
	public void checkFull(Oder a) {
		 if(nbOrders <= MAX_LIMITTED_ORDERS) {
			 for(int i = 1; i <= nbOrders;i++) {
				 System.out.println(itemsOdered[i].getTitle() + " has beeen added.");
			 }
		 }
		 else {
			 for(int i = 1; i <= nbOrders;i++) {
				if(i <= MAX_LIMITTED_ORDERS) {
					System.out.println(itemsOdered[i].getTitle() + " has beeen added.");
				}
				else {
					System.out.println(itemsOdered[i].getTitle() + " has not beeen added.");
					//removeDigitalVideoDisc(itemsOdered[i]);
				}
			 }
	
		 }
		 System.out.println("\n");
	}
	public int getQtyOdered() {
		return qtyOdered;
	}
	public void setQtyOdered(int qtyOdered) {
		this.qtyOdered = qtyOdered;
	}
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if(qtyOdered < MAX_NUMBER_ORDERED) {
			nbOrders++;
			itemsOdered[qtyOdered] = disc;
			qtyOdered=qtyOdered+1;
		}
		else {
			System.out.println("Full.Can not add item");
		}
	}
	public void addDigitalVideoDisc(DigitalVideoDisc [] dvdList) {
		if(qtyOdered + dvdList.length > MAX_NUMBER_ORDERED) {
			System.out.println("Loi");
		}
		for(int i = 1 ; i <= dvdList.length ; ++i) {
			nbOrders++;
         this.addDigitalVideoDisc(dvdList[i]);			
		}
	}
	public void addDigitalVideoDisc(DigitalVideoDisc dvd1,DigitalVideoDisc dvd2) {
		if(qtyOdered + 2 > MAX_NUMBER_ORDERED) {
			System.out.println("Loi");
		}
		else {
			nbOrders++;
			this.addDigitalVideoDisc(dvd1);
			this.addDigitalVideoDisc(dvd2);
		}
	}
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		for(int j = 0; j < qtyOdered - 1; j++) {
			if(itemsOdered[j] == disc) {
				for(int i = j; i< qtyOdered; i++) {
					itemsOdered[j]=itemsOdered[j+1];
				}
				qtyOdered--;
			}
		}		
	}
	public boolean Search(String title) {
		for(int i = 0; i < nbOrders;i++) {
		if(itemsOdered[i].getTitle().contains(title)) {
			System.out.println("true");
		}
		}
		return false;
	}
	public void show(Oder a) {
		float totalCost = 0.0f;
		System.out.println("******************************Order**********************************************");
		System.out.println("Date: "+dateOrdered.getDay()+"/"+dateOrdered.getMonth()+"/"+dateOrdered.getYear());
		System.out.println("Ordered Items:");
		if(qtyOdered <= MAX_LIMITTED_ORDERS+1) {
			for(int i = 0;i < qtyOdered;i++) {
			System.out.println(i+1 +".DVD" + " - " + itemsOdered[i].getTitle() + " - " + itemsOdered[i].getCategory() + " - " + itemsOdered[i].getDirector() + " - " + itemsOdered[i].getLength()+": "+ itemsOdered[i].getCost());
			totalCost = totalCost + itemsOdered[i].getCost();
			}
		}
		else {
			for(int i = 0; i < nbOrders;i++) {
				if(i <= MAX_LIMITTED_ORDERS+1) {
					System.out.println(i+1 +".DVD" + " - " + itemsOdered[i].getTitle() + " - " + itemsOdered[i].getCategory() + " - " + itemsOdered[i].getDirector() + " - " + itemsOdered[i].getLength()+": "+ itemsOdered[i].getCost());
					totalCost = totalCost + itemsOdered[i].getCost();
				}
				else {
					removeDigitalVideoDisc(itemsOdered[i]);
				}
		}	
		}
		System.out.println("Total cost: "+ totalCost);
		System.out.println("**********************************************************************************");
	}	
	public void Compare(String a) {
		String str = new String();
		String str1 = new String();
		StringTokenizer st = new StringTokenizer(a);
		while(st.hasMoreTokens()){
			 str = st.nextToken();
			 
		}
		for(int i = 0;i < qtyOdered;i++) {
			
			String b = itemsOdered[i].getTitle(); 
			StringTokenizer st1 = new StringTokenizer(b);
			while(st1.hasMoreTokens()){			
				str1 = st1.nextToken();
			}
			if(str.equalsIgnoreCase(str1)) {
				System.out.println(itemsOdered[i].getTitle());
			}
		}
		
	}
	public void DigitalVideoDiscgetALuckyItem() {
		Random generator = new Random ();
		int value = generator.nextInt(nbOrders);
		System.out.println("San pham duoc mien phi la DVD: "+itemsOdered[value].getTitle());
		itemsOdered[value].setCost(0);
	}
}
