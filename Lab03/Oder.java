public class Oder {
	public static final int MAX_NUMBER_ORDERED = 10;
	private int qtyOdered;
	private DigitalVideoDisc itemsOdered[] = new DigitalVideoDisc[MAX_NUMBER_ORDERED];
	public int getQtyOdered() {
		return qtyOdered;
	}
	public void setQtyOdered(int qtyOdered) {
		this.qtyOdered = qtyOdered;
	}
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if(qtyOdered < MAX_NUMBER_ORDERED) {
			itemsOdered[qtyOdered] = disc;
			qtyOdered=qtyOdered+1;
		}
		else {
			System.out.println("Full.Can not add item");
		}
	}
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		for(int j = 0; j < qtyOdered - 1; j++) {
			if(itemsOdered[j] == disc) {
				for(int i = j; i< qtyOdered; i++) {
					itemsOdered[j]=itemsOdered[j+1];
				}
				qtyOdered--;
			}
		}		
	}
	public float totalCost() {
		float totalCost = 0.0f;
		for(int j = 0; j < qtyOdered; j++) {
			totalCost = totalCost + itemsOdered[j].getCost();
		}
		return totalCost;
	}
}
