package hust.soict.hedspi.lab02;
import java.util.Scanner;

public class PrintTriangle {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		int height = 0;
		
		// get height
		do {
			System.out.print("Enter height for the triangle: h = ");
			height = scanner.nextInt();
			if (height <= 1) {
				System.out.println("Invalid height. Re-enter height > 1.");
			}
		} while (height <= 1);
		
		// print the triangle
		for(int i = 1; i <= height; i++) {
			for(int k = 1; k <= height - i; k++) {
				System.out.print(' ');	
			}
			for(int j = 1; j <= 2 * i - 1; j++) {
				System.out.print('*');	
			}
			System.out.println("");
		}
		
		scanner.close();
		System.exit(0);
		
	}

}
