package hust.soict.hedspi.lab02;
import javax.swing.JOptionPane;

public class ChoosingOption {

	public static void main(String[] args) {
		
		int option = JOptionPane.showConfirmDialog(null, 
				"Do you want to change to the first class ticket?");
		
		JOptionPane.showMessageDialog(null, 
				"You've chosen: " + 
				(option == JOptionPane.YES_NO_OPTION? "Yes" : "No"));
		
		// if users chose "Cancel" -> the result will be the same as "No"
		// because the user didn't choose "Yes"
		
		// to CUSTOMIZE THE OPTIONS to users:
		// showOptionDialog(Component parentComponent,
        //                  Object message,
        //                  String title,
        //                  int optionType,
        //                  int messageType,
        //                  Icon icon,
        //                  Object[] options,
        //                  Object initialValue)
		
		String[] answers = {"Agree", "Disagree"};
		
		JOptionPane.showOptionDialog(null,
		        "This is a way customize the options to users.\n" +
		        "Very useful, don't you agree?", "A message title",
		        JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, 
		        answers, answers[0]);
		
		System.exit(0);
		
	}

}
