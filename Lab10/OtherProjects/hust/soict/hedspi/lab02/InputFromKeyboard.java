package hust.soict.hedspi.lab02;
import java.util.Scanner;

public class InputFromKeyboard {

	public static void main(String[] args) {

		Scanner keyboard = new Scanner(System.in);

		System.out.print("What's your name?\n- ");
		String strName = keyboard.nextLine();
		
		System.out.print("How old are you?\n- ");
		int iAge = keyboard.nextInt();
		
		System.out.print("How tall are you?\n- ");
		double dHeight = keyboard.nextDouble();
		
		System.out.println("Hello Mrs/Ms." + strName + ", " +
							iAge + " years old. " + 
							"Your height is " + dHeight + ".");
		
		keyboard.close();
		System.exit(0);

	}

}
