package hust.soict.hedspi.lab02;
import java.util.Scanner;


public class DaysOfMonth2 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        MyDate date = new MyDate();
        String month, year;
        boolean validInput = false;
        
        // the exercise
        System.out.println("Enter a month and a year and i will " +
        "calculate and display the number of days of that month.\n");
        
        // get month
        do {
            System.out.print("Enter a month: ");
            month = scanner.nextLine();
            // check the validity of the input
            validInput = date.setMonth(month);
            if (validInput == false) {
                System.out.println("Invalid month. " +
                        "For instance, the valid forms for January are \"January\", \"Jan.\", \"Jan\" or \"1\"." +
                        "\nPlease re-enter valid input.\n");
            }
        } while (validInput == false);
        
        // get year
        do {
            System.out.print("Enter a year: ");
            year = scanner.nextLine();
            // check the validity of the input
            validInput = date.setYear(year);
            if (validInput == false) {
                System.out.println("Invalid year. " +
                        "The valid form is a non-negative number and is entered all the digit." +
                        "\nFor instance, the valid input of the year 1999 is only 1999." +
                        "\nPlease re-enter valid input.\n");
            }
        } while (validInput == false);
        
        // print result
        System.out.println("The number of days in " +
                    date.month + "/" + date.year + " is " + date.numberOfDays + " days.");
        
        scanner.close();
        System.exit(0);
        
    }
    
}


class MyDate {
    
    public int numberOfDays;
    public int month;
    public int year;
    
    public boolean setMonth(String month) {
        switch(month) {
        case "January": case "Jan.": case "Jan": case "1":  this.month = 1;
        case "March": case "Mar.": case "Mar": case "3":    this.month = 3;
        case "May": case "May.": case "5":                  this.month = 5;
        case "July": case "Jul.": case "Jul": case "7":     this.month = 7;
        case "August": case "Aug.": case "Aug": case "8":   this.month = 8;
        case "October": case "Oct.": case "Oct": case "10": this.month = 10;
        case "December": case "Dec.": case "Dec": case "12":this.month = 12;
            this.numberOfDays = 31;
            return true;
        case "April": case "Apr.": case "Apr": case "4":    this.month = 4;
        case "June": case "Jun.": case "Jun": case "6":     this.month = 6;
        case "September": case "Sep.": case "Sep": case "9":this.month = 9;
        case "November": case "Nov.": case "Nov": case "11":this.month = 11;
            this.numberOfDays = 30;
            return true;
        case "February": case "Feb.": case "Feb": case "2": this.month = 2;
            this.numberOfDays = 28; // special case -> re-calculation if isLeapYear = true
            return true;
        default:
            return false;
        }
    }
    
    public boolean setYear(String year) {
        this.year = Integer.parseInt(year);
        if (this.year < 0) {
            return false;
        } else {
            if (this.month == 2 && isLeapYear() == true) {
                this.numberOfDays = 29;
            }
            return true;
        }
    }
    
    public boolean isLeapYear() {
        if ((this.year % 400) == 0) {
            return true;
        } else if ((this.year % 100) != 0 && (this.year % 4) == 0) {
            return true;
        } else {
            return false;
        }
    }
    
}