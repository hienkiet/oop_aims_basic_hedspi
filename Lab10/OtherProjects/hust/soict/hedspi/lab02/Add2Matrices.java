package hust.soict.hedspi.lab02;
public class Add2Matrices {

	public static void main(String[] args) {
	    
	    final int A = 3; // number of rows
	    final int B = 3; // number of columns

		System.out.println("Adding two constants matrices of the same size Program\n");
		
		int m1[][] = { {1, 2, 3},
		               {4, 5, 6},
		               {7, 8, 9} };
		int m2[][] = { {9, 2, 3},
		               {8, 1, 4},
		               {7, 6, 5} };
		
		// print the 2 constants matrices
		System.out.println("The two constants matrices:\n" + "\nm1");
		printMatrix(m1, A, B);
		System.out.println("\nm2:");
		printMatrix(m2, A, B);
		
		// print the result after adding 2 the matrices
		System.out.println("\nThe result of (m1 + m2):");
		int m3[][] = add2Matrices(m1, m2, A, B);
		printMatrix(m3, A, B);
		
		System.exit(0);
	}
	
	private static void printMatrix(int m[][], int a, int b) {
	    
	    for (int i = 0; i < a; i++) {
	        for (int j = 0; j < b; j++) {
	            System.out.printf("%-3d ", m[i][j]);
	        }
	        System.out.println("");
	    }
	}
	
	private static int[][] add2Matrices(int m1[][], int m2[][], int a, int b) {
	    
        int result[][] = new int[a][b];
        
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                result[i][j] = m1[i][j] + m2[i][j];
            }
        }
        
        return result;
	    
    }

}
