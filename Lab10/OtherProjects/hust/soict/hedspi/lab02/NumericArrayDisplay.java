package hust.soict.hedspi.lab02;
import java.util.Scanner;
import java.util.Arrays;

public class NumericArrayDisplay {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		int size = 0;
		
		// the exercise
		System.out.println("A numeric array display\n" +
		"with sorting, calculating the sum and average value of the elements\n");
		
		// get array size
		do {
			System.out.print("Enter the size of the array: ");
			size = scanner.nextInt();
			if (size < 1) {
				System.out.println("Invalid size. " +
						"The valid size is a non-negative integer.\n" +
						"Please re-enter valid size.\n");
			}
		} while (size < 1);
		
		// get array elements' value
		int numbers[] = new int[size];
		
		for (int i = 0; i < size; i++) {
			System.out.print("Enter element no." + i + "'s value: ");
			numbers[i] = scanner.nextInt();
		}
		
		// print the array
		System.out.println("\nThe the array you have inputted:\n" + Arrays.toString(numbers));
		
		// bubble sort the array
		Arrays.sort(numbers);
		
		// print the sorted array
		System.out.println("The sorted array:\n" + Arrays.toString(numbers));
		
		// print the sum and average
		int sum = Arrays.stream(numbers).sum();
		System.out.println("\nThe sum value of the array elements: " + sum +
						"\nThe average value of the array elements: " + ((double)sum / numbers.length));
		
		scanner.close();
		System.exit(0);

	}
	
}
