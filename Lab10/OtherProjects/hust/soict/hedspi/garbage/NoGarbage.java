package hust.soict.hedspi.garbage;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class NoGarbage {

    public static void main(String[] args) throws FileNotFoundException {
        File myFile = new File("OtherProjects\\hust\\soict\\hedspi\\garbage\\VeryLongTextFile.txt");
        Scanner myScanner = new Scanner(myFile);
        StringBuffer sb = new StringBuffer();
        long start = System.currentTimeMillis();
        
        while (myScanner.hasNextLine()) {
            sb.append(myScanner.nextLine());
            sb.append('\n');
        }
        myScanner.close();
        
        System.out.println(sb.toString());
        System.out.print("Using StringBuffer cost ");
        System.out.print((System.currentTimeMillis() - start) + " milliseconds");
        
        System.exit(0);
    }

}
