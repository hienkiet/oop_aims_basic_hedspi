package hust.soict.hedspi.garbage;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class GarbageCreator {

    public static void main(String[] args) throws FileNotFoundException {
        File myFile = new File("OtherProjects\\hust\\soict\\hedspi\\garbage\\VeryLongTextFile.txt");
        Scanner myScanner = new Scanner(myFile);
        String str = "";
        long start = System.currentTimeMillis();
        
        while (myScanner.hasNextLine()) {
            str += myScanner.nextLine();
            str += '\n';
        }
        myScanner.close();
        
        System.out.println(str);
        System.out.print("Using String cost ");
        System.out.print((System.currentTimeMillis() - start) + "milliseconds");
        
        System.exit(0);
    }

}
