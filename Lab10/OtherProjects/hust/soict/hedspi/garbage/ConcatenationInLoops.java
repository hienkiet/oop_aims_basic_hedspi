package hust.soict.hedspi.garbage;

import java.util.Random;

public class ConcatenationInLoops {

    public static void main(String[] args) {
        // String
        Random r = new Random(123);
        long start = System.currentTimeMillis();
        @SuppressWarnings("unused")
        String s = "";
        for (int i = 0; i < 65536; i++) {
            s += r.nextInt(2);
        }
        System.out.print(System.currentTimeMillis() - start);
        System.out.println(" - String");
        
        // StringBuilder.
        r = new Random(123);
        start = System.currentTimeMillis();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 65536; i++) {
            sb.append(r.nextInt(2));
        }
        s = sb.toString();
        System.out.print(System.currentTimeMillis() - start);
        System.out.println(" - StringBuilder");
        
        // StringBuffer
        r = new Random(123);
        start = System.currentTimeMillis();
        StringBuffer sbf = new StringBuffer();
        for (int i = 0; i < 65536; i++) {
            sbf.append(r.nextInt(2));
        }
        s = sbf.toString();
        System.out.print(System.currentTimeMillis() - start);
        System.out.println(" - StringBuffer");
        
    }

}
