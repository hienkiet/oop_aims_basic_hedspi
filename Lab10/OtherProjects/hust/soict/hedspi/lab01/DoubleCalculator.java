package hust.soict.hedspi.lab01;
import java.util.Scanner;

public class DoubleCalculator {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		double x1, x2;
		
		System.out.println("Simple calculator for two double number\n");
		
		// input
		System.out.print("Enter the first double x1 = "); x1 = scanner.nextDouble();
		do {
			System.out.print("Enter the second double x2 = "); x2 = scanner.nextDouble();
			if (x2 == 0) {
				System.out.println("The x2 can not be 0. " + 
								"Because x1 will be the dividend and x2 will be the divisor.\n" +
								"Please re-enter.");
			}
		} while (x2 == 0);
		
		// calculate & print result
		System.out.println("\nResult:");
		System.out.println("The sum: x1 + x2 = " + (x1 + x2));
		System.out.println("The difference: x1 - x2 = " + (x1 - x2));
		System.out.println("The product: x1 * x2 = " + (x1 * x2));
		System.out.println("The quotient: x1 / x2 = " + (x1 / x2));
		
		scanner.close();
		System.exit(0);
		
	}

}
