package hust.soict.hedspi.lab01;
import java.util.Scanner;

public class FirstDegreeEquation2Vars {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		double a11, a12, b1, x1;
		double a21, a22, b2, x2;
		double d, d1, d2;
		
		System.out.println("First-degree equation with two variables Calculator\n\n" +
				"A system of first-degree equations with two variables x1 and x2 can be written as follows.\n" +
				"a11 * x1 + a12 * x2 = b1\n" +
				"a21 * x1 + a22 * x2 = b2\n" +
				"where a11, a12, a21, a22, b1 and b2 are coefficients.\n");

		// input
		System.out.print("Enter a11 = ");	a11 = scanner.nextDouble();
		System.out.print("Enter a12 = ");	a12 = scanner.nextDouble();
		System.out.print("Enter b1  = ");	b1 = scanner.nextDouble();
		System.out.print("Enter a21 = "); 	a21 = scanner.nextDouble();
		System.out.print("Enter a22 = ");	a22 = scanner.nextDouble();
		System.out.print("Enter b2  = ");	b2 = scanner.nextDouble();

		// calculate the determinants
		d = a11 * a22 - a21 * a12;
		d1 = b1 * a22 - b2 * a12;
		d2 = a11 * b2 - a21 * b1;
		
		// print result
		if (d != 0) {
			x1 = d1 / d;
			x2 = d2 / d;
			System.out.println("\nThe equation has an unique solution (x1,x2) = ("
							+ x1 + "," + x2 + ").");
		} else if (d == 0 && d1 == 0 && d2 == 0) {
			System.out.println("\nThe equation has infinitely many solutions.");
		} else {
			System.out.println("\nThe equation has no solutions.");
		}
		
		scanner.close();
		System.exit(0);

	}

}
