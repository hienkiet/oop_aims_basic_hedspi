package hust.soict.hedspi.lab01;
import java.util.Scanner;

public class FirstDegreeEquation1Var {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		double a, b, x;
		
		System.out.println("First-degree equation with one variable Calculator\n\n" +
							"A first-degree equation with one variable can have the form such as\n" +
							"ax + b = 0 (a != 0)\n" +
							"where x is the variable, and a and b are coefficients.\n");
		
		// input
		do {
			System.out.print("Enter a = ");	a = scanner.nextDouble();
			if (a == 0) {
				System.out.println("a can not be 0. Please re-enter.");
			}
		} while (a == 0);
		System.out.print("Enter b = "); b = scanner.nextDouble();
		
		// calculate & print result
		x = - b / a;
		System.out.println("The equation has an unique solution x = " + x);

		scanner.close();
		System.exit(0);

	}

}
