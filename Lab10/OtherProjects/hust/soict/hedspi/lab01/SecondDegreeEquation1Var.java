package hust.soict.hedspi.lab01;
import java.util.Scanner;
import java.lang.Math;

public class SecondDegreeEquation1Var {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		double a, b, c, d, x1, x2;
		
		System.out.println("Second-degree equation with one variable Calculator\n\n" +
				"A second-degree equation with one variable can have the form such as\n" +
				"a*x + b*x*x + c = 0 (a != 0)\n" +
				"where a, b and c are coefficients.\n");

		// input
		do {
			System.out.print("Enter a = ");	a = scanner.nextDouble();
			if (a == 0) {
				System.out.println("a can not be 0 or the equation will be come a first-degree equation\n" +
								"Please re-enter.");
			}
		} while (a == 0);
		System.out.print("Enter b = ");	b = scanner.nextDouble();
		System.out.print("Enter c = ");	c = scanner.nextDouble();
		
		// calculate & print result
		d = b * b - 4 * a * c;
		if (d == 0) {
			x1 = x2 = - b / (2 * a);
			System.out.println("\nThe equation has double root x = " + x1);
		} else if (d > 0) {
			x1 = (-b + Math.sqrt(d)) / (2 * a);
			x2 = (-b - Math.sqrt(d)) / (2 * a);
			System.out.println("\nThe equation has two distinct roots:\n" +
								"x1 = " + x1 + "\n" +
								"x2 = " + x2);
		} else {
			System.out.println("\nThe equation has no solutions.");
		}
		
		scanner.close();
		System.exit(0);
		
	}

}
