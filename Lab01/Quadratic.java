import javax.swing.JOptionPane;
public class Quadratic {
public static void main(String[] args) {
	String astr,bstr,cstr,kq;
	
	astr = JOptionPane.showInputDialog(null, "He so a : "," a*x^2 + b*x + c = 0",JOptionPane.INFORMATION_MESSAGE);
	bstr = JOptionPane.showInputDialog(null, "He so b : "," a*x^2 + b*x + c = 0",JOptionPane.INFORMATION_MESSAGE);
	cstr = JOptionPane.showInputDialog(null, "He so c : "," a*x^2 + b*x + c = 0",JOptionPane.INFORMATION_MESSAGE);

	
	double a = Double.parseDouble(astr);
	double b = Double.parseDouble(bstr);
	double c = Double.parseDouble(cstr);
	double delta = b*b - 4*a*c;
	double sqrtDelta = Math.sqrt( b*b - 4*a*c);
	if(delta < 0 ) {
		JOptionPane.showMessageDialog(null,"Phuong trinh vo nghiem !");
	}
	else if(delta == 0) {
		double x0 = -b /(2*a);
		JOptionPane.showMessageDialog(null,x0,"Phuong trinh co nghiem kep x0 = ",JOptionPane.INFORMATION_MESSAGE);
	}
	else {
		double x1 = (-b - sqrtDelta )/(2*a);
		double x2 = (-b + sqrtDelta )/(2*a) ;
		kq = x1 + " va " + x2;
		JOptionPane.showMessageDialog(null,kq,"Phuong trinh co hai nghiem = ",JOptionPane.INFORMATION_MESSAGE);
	}
}
}
