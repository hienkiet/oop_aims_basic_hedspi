import java.util.Arrays;
import java.util.Scanner;
public class ArraySort {
	public static void main(String[] args) {
		Scanner key = new Scanner(System.in);
		System.out.println("Nhap so phan tu cua mang :");
		int n = key.nextInt();
		int a[] = new int[n];
		for(int i=0; i < n;i++) {
			System.out.println("Nhap cac phan tu cua mang a["+ i +"] =");
			a[i] = key.nextInt();
		}
		Arrays.sort(a);
		System.out.println("Mang sau khi sap xep tang dan :");
		for(int x:a) {
			System.out.println(x);
		}
	}

}
