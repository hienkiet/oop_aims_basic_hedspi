//Cach 1
/*import java.util.Scanner;

public class Triangle {
	public static void main(String[] args) {
		int num;
		System.out.print("Số dòng: ");
		Scanner input = new Scanner(System.in);
		num = input.nextInt();
		for (int r = 1; r <= num; r++) {
			for (int sp = num - r; sp > 0; sp--) {
				System.out.print(" ");
			}
			for (int c = 1; c <= r; c++) {
				System.out.print('*');
			}
			for (int k = 2; k <= r; k++) {
				System.out.print('*');
			}
			System.out.println();
		}
	}
}*/
//Cach 2
import java.util.Scanner;

public class Triangle{
	public static void main(String[] args) {
		int num;
		System.out.println("Số dòng:");
		Scanner input = new Scanner(System.in);
		num = input.nextInt();
		for(int i = 1 ; i <= num; i++ ) {
			for(int j = 1; j <= 2*num-1 ;j++) {
				if(j <= num - i) 
				System.out.print(" ");
				else if( j >= num + i)
					System.out.print(" ");
				else 
					System.out.print("*");
			}
			System.out.println();
		}
	}
}